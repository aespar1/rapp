FROM rocker/verse:4.2

RUN apt update -qq -y

RUN apt install -y software-properties-common dirmngr libcurl4-openssl-dev \
    libssl-dev libssh2-1-dev libxml2-dev zlib1g-dev make git-core \
    libcurl4-openssl-dev libxml2-dev libpq-dev cmake \
    r-base r-base-dev libsodium-dev libsasl2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev \
    libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev default-jre default-jdk \
    libhiredis-dev libssh-dev

RUN R CMD javareconf

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install

RUN R -e "install.packages('renv')"
# Install miniconda
ENV CONDA_DIR /opt/conda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
     /bin/bash ~/miniconda.sh -b -p /opt/conda

# Put conda in path so we can use conda activate
ENV PATH=$CONDA_DIR/bin:$PATH

WORKDIR /app

COPY renv.lock  .
RUN echo "options(production=TRUE)" >> /app/.Rprofile
RUN echo "options(docker=TRUE)" >> /app/.Rprofile
RUN echo "source('onload/activate.r')" >> /app/.Rprofile


COPY package.json package.json
RUN apt install npm nodejs -y
RUN npm install --global yarn
RUN yarn install

COPY onload onload

RUN R -e "renv::init()"
RUN R -e "renv::use_python(type='conda')"
RUN R -e "renv::restore()"
RUN R -e "reticulate::py_install('boto3')"

RUN mkdir /root/.ssh

COPY src src
COPY app.r app.r
