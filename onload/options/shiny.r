options(
  shiny.port = 8000,
  shiny.host = "0.0.0.0",
  shiny.maxRequestSize = 200,
  shiny.json.digits = 7,
  shiny.suppressMissingContextError = FALSE,
  shiny.table.class = NULL,
  shiny.fullstacktrace = FALSE,
  shiny.maxRequestSize = 30 * 1024^2
)

if (isFALSE(getOption("production"))) {
  message("Shiny Development Mode")
  options(
    shiny.host = "127.0.0.1",
    shiny.autoreload = FALSE,
    shiny.reactlog = FALSE,
    shiny.minified = FALSE,
    # shiny.error = utils::recover,
    shiny.deprecation.messages = TRUE,
    shiny.sanitize.errors = FALSE
  )
}
