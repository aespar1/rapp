Sys.setenv(REDIS_PORT = 6379)
if (getOption("production")) {
  Sys.setenv(REDIS_HOST = "redis")
} else {
  Sys.setenv(REDIS_HOST = "127.0.0.1")
}
