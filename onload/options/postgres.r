Sys.setenv(
  POSTGRES_PORT = 5432,
  POSTGRES_USER = "user",
  POSTGRES_PASSWORD = "password",
  POSTGRES_DB = "db"
)

if (isTRUE(options("production"))) {
  Sys.setenv(
    POSTGRES_HOST = "127.0.0.1"
  )
} else {
  Sys.setenv(
    POSTGRES_HOST = "postgres"
  )
}
