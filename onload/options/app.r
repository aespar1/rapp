if (getOption('docker')) {
  Sys.setenv(appcache='/srv/ndexr/')
} else {
  Sys.setenv(appcache='~/.cache/ndexr')
}
