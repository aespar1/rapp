#' @export
server <- function(input, output, session) {
  box::use(. / r / frontend)
  frontend$server_frontend("frontend")
}
