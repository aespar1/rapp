
#' @export
ui_terminal <- function(id) {
  {
    box::use(shiny = shiny[tags])
    box::use(shinyAce)
    box::use(. / terminal)
  }
  ns <- shiny$NS(id)

  tags$div(
    class = "container m-0 p-0",
    tags$div(
      class = "btn-toolbar d-flex justify-content-between bg-dark",
      role = "toolbar",
      `aria-label` = "Toolbar with button groups",
      tags$div(),
      tags$div(
        class = "btn-group-md", role = "group", `aria-label` = "Third group",
        tags$button(
          id = ns("restart"), type = "button", class = "btn action-button",
          tags$i(class = "bi-arrow-repeat text-light")
        )
      )
    ),
    tags$div(
      class = "row vh-100", style = "overflow-y: auto;",
      tags$div(
        class = "col-md-4",
        shinyAce$aceEditor(
          outputId = ns("code"),
          minLines = 10,
          mode = "r",
          theme = "crimson_editor",
          height = "100%",
          autoComplete = "live",
          autoCompleters = "rlang",
          vimKeyBinding = TRUE,
          showLineNumbers = TRUE,
          hotkeys = list(
            runKey = list(
              win = "Ctrl-Enter",
              mac = "CMD-ENTER"
            )
          )
        )
      ),
      tags$div(
        class = "col-md-6",
        shiny$uiOutput(ns("markdownOutput"))
      )
    )
  )
}

#' @export
server_terminal <- function(id) {
  {
    box::use(shiny = shiny[tags])
    box::use(shinyAce)
    box::use(jsonlite)
    box::use(knitr)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      shinyAce$aceAutocomplete("code")

      output$codeOutput <- shiny$renderPrint({
        if (is.null(input)) {
          return("Execute [R] chunks with Ctrl/Cmd-Enter")
        } else {
          tryCatch(expr = {
            rlang$eval_tidy(rlang$parse_expr(input$code_runKey$selection))
          }, error = function(err) {
            as.character(err)
          })
        }
      })
    }
  )
}
