#' @export
app_router <- function(ns) {
  box::use(shiny.router)
  box::use(. / main / main)
  box::use(. / terminal / terminal)
  box::use(. / aws / aws)
  box::use(. / git / git)
  shiny.router$make_router(
    shiny.router$route("aws", aws$ui_aws(ns("aws"))),
    shiny.router$route("main", main$ui_main(ns("main"))),
    shiny.router$route("terminal", terminal$ui_terminal(ns("terminal")))
    # shiny.router$route("git", git$ui_gitlab_credentials(ns("gitlab")))
  )
}
