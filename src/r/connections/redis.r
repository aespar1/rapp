#' @export
connection_redis <- function() {
  box::use(redux)
  con <- redux$hiredis(host = Sys.getenv("REDIS_HOST"), port = Sys.getenv("REDIS_PORT"))
}

#' @export
store_state <- function(id, data) {
  box::use(. / redis)
  box::use(glue[glue])
  box::use(redux)
  box::use(shiny)
  clearRedis <- getOption("clear_redis", FALSE)
  if (clearRedis) con$DEL(ns(credentials()$user))
  con <- redis$connection_redis()
  data <- redux$object_to_bin(data)
  con$SET(key = id, value = data)
}

#' @export
get_state <- function(id) {
  box::use(. / redis)
  box::use(glue[glue])
  box::use(redux)
  box::use(shiny)
  cli::cli_alert_info("Storing {id}")
  message(glue("Storing data {id}"))
  con <- redis$connection_redis()
  data <- con$GET(key = id)
  if (!is.null(data)) data <- redux$bin_to_object(data)
  if (shiny$is.reactivevalues(data)) {
    return(list())
  }
  data
}
