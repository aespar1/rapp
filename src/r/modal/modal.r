#' @export
modal_error <- function(err) {
  box::use(shiny)
  shiny$showModal(
    shiny$modalDialog(
      size = "xl",
      shiny$tags$pre(
        shiny$tags$code(
          as.character(err)
        )
      )
    )
  )
}
