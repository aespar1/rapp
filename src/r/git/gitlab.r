#' @export
gitlab_set_auth <- function(project_id = "41989252") {
  box::use(gitlabr)
  gitlabr$set_gitlab_connection(
    gitlab_url = "https://gitlab.com/",
    private_token = Sys.getenv("GITLAB_PAT")
  )
  # as.data.frame(gitlabr$gl_list_files(project_id))
  gitlabr$gl_get_file(project_id, ".gitlab-ci.yml")
}

# gitlab_set_auth()
