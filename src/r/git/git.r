
#' @export
ui_gitlab_credentials <- function(id = "gitlab_credentials", ns_common_store_user) {
  {
    box::use(shiny = shiny[tags])
    box::use(.. / state / setDefault[setDefault])
    box::use(.. / connections / redis)
  }
  ns <- shiny$NS(id)
  gitlab_credentials <- redis$get_state(ns_common_store_user("gitlab_credentials"))

  gitlabAccess <- setDefault(gitlab_credentials$gitlabAccess, Sys.getenv("GITLAB_PAT"))

  tags$div(
    class = "card",
    tags$div(class = "card-header", tags$h5("gitlablab Credentials", class = "card-title text-dark p-1")),
    tags$div(
      class = "card-body",
      shiny$passwordInput(
        ns("gitlablabPat"), shiny$tags$b("Gitlab Personal Access Token"), gitlabAccess
      )
    ),
    tags$div(
      class = "card-footer d-flex justify-content-around",
      shiny$actionButton(
        ns("update"), shiny$tags$b("Update"),
        class = "btn btn-sm btn-block btn-primary "
      )
    )
  )
}

#' @export
server_gitlab_credentials <- function(id = "gitlab_credentials", ns_common_store_user) {
  {
    box::use(shiny)
    box::use(.. / connections / redis)
    box::use(shiny)
    box::use(. / client)
    box::use(stringr)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      shiny$observeEvent(input$update, {
        input <- shiny$reactiveValuesToList(input)
        input <- lapply(input, function(x) gsub(" ", "", x))
        redis$store_state(ns_common_store_user("gitlab_credentials"), input)
        shiny$showNotification("Credentials Updated")
        input
      })
    }
  )
}
