#' @export
ui_sidebar_item <- function(name, icon) {
  box::use(shiny[tags])
  box::use(shiny.router[route_link])
  tags$ul(
    class = "nav nav-pills nav-flush flex-sm-column flex-row flex-nowrap mb-auto mx-auto text-center align-items-center", # nolint
    tags$li(
      class = "nav-item",
      tags$a(
        href = route_link(tolower(name)),
        class = "nav-link py-3 px-2",
        title = icon,
        `data-bs-toggle` = "tooltip",
        `data-bs-placement` = "right",
        `data-bs-original-title` = name,
        tags$i(
          class = icon
        )
      )
    )
  )
}

#' @export
ui_sidebar <- function(id = "sidebar") {
  box::use(shiny = shiny[tags])
  box::use(shiny.router[route_link])
  box::use(. / sidebar)
  ns <- shiny$NS(id)
  tags$div(
    class = "d-flex flex-md-column flex-row flex-nowrap bg-light align-items-center sticky-top", # nolint
    tags$a(
      href = route_link("main"),
      class = "d-block link-dark text-decoration-none",
      title = "",
      `data-bs-toggle` = "tooltip",
      `data-bs-placement` = "right",
      `data-bs-original-title` = "Icon-only",
      tags$i(
        class = "bi-house fs-4"
      )
    ),
    sidebar$ui_sidebar_item("aws", "bi-cpu fs-4"),
    sidebar$ui_sidebar_item("terminal", "bi-terminal fs-4"),
    sidebar$ui_sidebar_item("git", "bi-git fs-4")

    # tags$div(
    #   class = "dropdown",
    #   tags$a(
    #     href = "#",
    #     class = "d-flex align-items-center justify-content-center p-3 link-dark text-decoration-none dropdown-toggle", # nolint
    #     id = "dropdownUser3",
    #     `data-bs-toggle` = "dropdown",
    #     `aria-expanded` = "false",
    #     tags$i(
    #       class = "bi-brush fs-4"
    #     )
    #   ),
    #   tags$ul(
    #     class = "dropdown-menu text-small shadow",
    #     `aria-labelledby` = "dropdownUser3",
    #     tags$li(
    #       shiny$actionButton(ns("bgRandom"), "Randomly Color <div>", class = "dropdown-item") # nolint
    #     )
    #   )
    # )
  )
}

#' @export
server_sidebar <- function(id) {
  box::use(shiny)
  box::use(shinyjs = shinyjs[js])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      shiny$observeEvent(input$bgRandom, {
        js$bgRandom()
      })
    }
  )
}
