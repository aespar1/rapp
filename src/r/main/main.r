#' @export
ui_main <- function(id) {
  box::use(shiny = shiny[tags])
  box::use(readr)
  box::use(jsonlite)
  pkg_json <- jsonlite$fromJSON("../package.json")
  pkg_json$scripts$devport <- NULL
  pkg_json <- jsonlite$toJSON(pkg_json, pretty = T)
  ns <- shiny$NS(id)
  shiny$withTags(
    div(
      class = "container",
      class = "row",
      div(
        class = "col-12 d-flex justify-content-around",
        p("Welcome to ndexr", class = "fw-bold"),
        p("Interactive:", as.character(interactive())),
        p("Application working directory: ", as.character(getwd()))
      ),
      div(
        class = "col-md-6",
        p(pre(pkg_json, class = "text-break"))
      ),
      div(
        class = "col-md-6",
        p(pre(jsonlite$toJSON(as.list(Sys.getenv()), pretty = T), class = "text-break"))
      )
    )
  )
}

#' @export
server_main <- function(id) {
  box::use(shiny)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
    }
  )
}
