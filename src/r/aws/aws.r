#' @export
ui_aws <- function(id = "aws", credentials = list(user = "default")) {
  {
    box::use(shiny = shiny[tags])
    box::use(dplyr)
    box::use(purrr)
    box::use(glue)
    box::use(shinyjs)
    box::use(. / ec2 / ec2)
    box::use(readr[read_file])
    box::use(. / ec2 / key_file)
    box::use(. / s3 / s3)
    box::use(. / s3 / s3_list_buckets)
    box::use(. / ec2 / security_groups)
    box::use(. / ec2 / rbox_init)
    box::use(. / ec2 / current_servers)
    box::use(. / aws_credentials)
    box::use(. / aws_credentials)
    box::use(. / costs)
    box::use(. / ec2 / instance_types_get)
  }
  ns <- shiny$NS(id)
  ns_common_store_user <- shiny$NS(credentials$user)

  tags$div(
    class = "container-fluid",
    tags$div(
      class = "row",
      tags$div(
        class = "col-12 d-flex justify-content-end align-items-center border-bottom bg-light",
        shiny$tags$a(class = "link-dark", href = "https://www.linkedin.com/in/freddydrennan/", target = "_blank", shiny$icon("linkedin")),
        shiny$tags$a(class = "link-dark", href = "https://github.com/fdrennan", target = "_blank", shiny$icon("github")),
        shiny$tags$a(class = "link-dark", href = "https://gitlab.com/fdrennan", target = "_blank", shiny$icon("gitlab"))
      ),
      tags$div(
        class = "col-12",
        tags$div(
          class = "row",
          tags$div(
            class = "col-lg-6 my-2",
            current_servers$ui_current_servers(ns("current_servers"))
          ),
          shiny$div(
            class = "col-sm-6 col-lg-3 my-2",
            costs$ui_aws_costs(ns("aws_costs"))
          ),
          shiny$div(
            class = "col-sm-6 col-lg-3 my-2",
            rbox_init$ui_rbox_init(ns("rbox_init"),
              ns_common_store_user = ns_common_store_user, instanceTypes = instance_types_get$instance_types_get()
            )
          )
        )
      ),
      tags$div(
        class = "row",
        tags$div(
          class = "col-sm-6 col-md-4 col-lg-3 my-2",
          aws_credentials$ui_aws_credentials(ns("aws_credentials"),
            ns_common_store_user = ns_common_store_user
          )
        ),
        tags$div(
          class = "col-sm-6 col-md-4 col-lg-3 my-2",
          tags$div(
            class = "pb-1",
            s3$ui_s3(ns("s3"), ns_common_store_user = ns_common_store_user)
          )
        ),
        tags$div(
          class = "col-sm-6 col-md-4 col-lg-3 my-2",
          key_file$ui_key_file(ns("key_file"), ns_common_store_user = ns_common_store_user)
        ),
        tags$div(
          class = "col-sm-6 col-md-4 col-lg-3 my-2",
          security_groups$ui_security_group(ns("security_group"),
            ns_common_store_user = ns_common_store_user
          )
        )
      )
    )
  )
}



#' @export
server_aws <- function(id = "aws", credentials = list(user = "default")) {
  {
    box::use(shiny = shiny[tags])
    box::use(dplyr)
    box::use(purrr)
    box::use(glue)
    box::use(shinyjs)
    box::use(. / ec2 / ec2)
    box::use(readr[read_file])
    box::use(. / ec2 / key_file)
    box::use(. / s3 / s3)
    box::use(. / s3 / s3_list_buckets)
    box::use(. / ec2 / security_groups)
    box::use(. / ec2 / rbox_init)
    box::use(. / ec2 / current_servers)
    box::use(. / aws_credentials)
    box::use(. / aws_credentials)
    box::use(. / costs)
    box::use(. / ec2 / instance_types_get)
  }

  # instance_types_get$instance_types_get()


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      ns_common_store_user <- shiny$NS(credentials$user)

      aws_credentials$server_aws_credentials(
        credentials = credentials,
        ns_common_store_user = ns_common_store_user
      )
      costs$server_aws_costs(ns_common_store_user = ns_common_store_user)
      s3$server_s3(ns_common_store_user = ns_common_store_user)
      current_servers$server_current_servers(ns_common_store_user = ns_common_store_user, instanceTypes = instance_types_get$instance_types_get())
      security_groups$server_security_group(ns_common_store_user = ns_common_store_user)
      key_file$server_key_file(ns_common_store_user = ns_common_store_user)

      rbox_init$server_rbox_init(
        credentials = credentials,
        ns_common_store_user = ns_common_store_user
      )
    }
  )
}
