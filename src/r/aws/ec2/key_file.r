#' @export
ui_key_file <- function(id = "key_file", ns_common_store_user) {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)

  box::use(.. / .. / connections / redis)
  box::use(.. / .. / state / setDefault[setDefault])
  redux <- redis$get_state(ns_common_store_user("key_file"))
  KeyName <- setDefault(redux$KeyName, "")

  tags$div(
    class = "card",
    tags$div(
      class = "card-header d-flex justify-content-between align-items-center",
      tags$h5("PEM Key", class = "card-title text-dark px-2"),
      tags$a(
        href = "https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html",
        shiny::icon("windows"), target = "_blank",
        class = "btn btn-sm btn-secondary btn-block"
      )
    ),
    tags$div(
      class = "card-body",
      shiny$textInput(ns("KeyName"), tags$b("Key Name"), KeyName)
    ),
    shiny$div(
      class = "card-footer d-flex justify-content-around",
      shiny$actionButton(ns("deleteKeyPair"), "Delete", class = "btn btn-sm btn-block btn-danger"),
      shiny$actionButton(ns("makeKeyPair"), "Create", class = "btn btn-sm btn-block btn-primary")
    )
  )
}

#' @export
server_key_file <- function(id = "key_file", ns_common_store_user) {
  {
    box::use(shiny)
    box::use(.. / client[client])
    box::use(. / key_pairs)
    box::use(glue)
    box::use(utils)
    box::use(fs)
    box::use(.. / .. / connections / redis)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      aws_credentials <- shiny$reactive({
        redis$get_state(ns_common_store_user("aws_credentials"))
      })

      ec2boto <- shiny$reactive({
        shiny$req(aws_credentials())
        client("ec2", aws_credentials = aws_credentials())
      })

      filename <- shiny$reactive({
        filename <- file.path("~", ".ssh", paste0(input$KeyName, ".pem"))
        fs$dir_create(fs$path_dir(filename), recurse = T)
        filename
      })


      shiny$observeEvent(input$makeKeyPair, {
        KeyName <- input$KeyName
        fileName <- paste0(KeyName, ".pem")
        if (isFALSE(KeyName %in% key_pairs$list_key_pair(aws_credentials = aws_credentials()))) {
          shiny$showNotification("Creating Key Pair")
          key_file <- key_pairs$manage_key_pair(KeyName, aws_credentials = aws_credentials())

          shiny$showModal(shiny$modalDialog(size = "xl", {
            shiny$fluidRow(
              shiny$column(
                12,
                shiny$tags$p(
                  shiny$tags$b("Download this file to ssh into your server."),
                  shiny$tags$em("You will have to create another keyfile if lost.")
                ),
                shiny$tags$p(
                  "Change access to the key in order to run ssh or autossh",
                  shiny$tags$pre(
                    shiny$tags$code(glue$glue("sudo chmod 400 {filename()}"))
                  )
                ),
                shiny$downloadButton(ns("downloadData"), "Download Key File")
              )
            )
          }))
        } else {
          shiny$showNotification("Key Pair already exists.")
        }

        output$downloadData <- shiny$downloadHandler(
          contentType = "application/x-x509-ca-cert",
          filename = function() {
            fileName
          },
          content = function(file) {
            write(key_file, file)
          }
        )
      })


      shiny$observeEvent(
        input$deleteKeyPair,
        {
          tryCatch(
            {
              ec2boto()$delete_key_pair(KeyName = input$KeyName)
              shiny$showNotification("Key pair deleted")
            },
            error = function(err) {
              shiny$showModal(shiny$modalDialog(size = "xl", shiny$tags$pre(shiny$tags$code(as.character(err)))))
            }
          )

          tryCatch(
            {
              file.remove(glue$glue(filename()))
            },
            error = function(err) {
              shiny$showModal(shiny$modalDialog(size = "xl", shiny$tags$pre(shiny$tags$code(as.character(err)))))
            }
          )
        }
      )

      shiny$observe({
        shiny$req(input$KeyName)
        input <- shiny$reactiveValuesToList(input)
        redis$store_state(ns_common_store_user("key_file"), input)
      })
    }
  )
}
