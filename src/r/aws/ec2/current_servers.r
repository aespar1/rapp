#' @export
ui_current_servers <- function(id = "current_servers") {
  box::use(shiny = shiny[tags])
  box::use(shinyWidgets)
  # box::use(shinycssloaders)
  ns <- shiny$NS(id)
  tags$div(
    class = "card",
    tags$div(
      class = "card-header d-flex justify-content-between align-items-center",
      tags$p("Active Servers"),
      tags$div(
        tags$a(
          href = "https://aws.amazon.com/ec2/pricing/on-demand/",
          shiny::icon("dollar"), target = "_blank",
          class = "btn btn-sm btn-secondary btn-block"
        ),
        shiny$actionButton(
          ns("pullServers"),
          shiny$icon("refresh"),
          class = "btn btn-sm btn-secondary btn-block"
        )
      )
    ),
    tags$div(
      class = "card-body",
      tags$div(
        class = "container-fluid",
        id = ns("serverPanel")
      )
    )
  )
}



#' @export
server_current_servers <- function(id = "current_servers", ns_common_store_user, instanceTypes) {
  box::use(shiny = shiny[tags])
  box::use(. / ec2)
  box::use(dplyr)
  box::use(glue[glue])
  box::use(uuid)
  box::use(. / instance)
  box::use(.. / .. / connections / redis)
  box::use(dplyr)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns



      shiny$observeEvent(input$pullServers, {

        aws_credentials <- redis$get_state(ns_common_store_user("aws_credentials"))
        instances <- instance$describe_instances(aws_credentials = aws_credentials)

        if (nrow(instances)==0) {
          shiny$showNotification('No known instances available.')
          shiny$req(FALSE)
        }

        if (!'PublicIpAddress' %in% names(instances)) {
          instances$PublicIpAddress = NA_character_
        }

        instances <-
          instances |>
          dplyr$filter(state != "terminated") |>
          dplyr$select(state, PublicIpAddress, KeyName, InstanceType, PublicDnsName, InstanceId)


        InstanceId <- instances$InstanceId


        shiny$removeUI(paste0("#", ns("servers")))
        shiny$insertUI(
          selector = paste0("#", ns("serverPanel")),
          where = "beforeEnd",
          ui = tags$div(class = "row", id = ns("servers"))
        )
        uuid <- uuid$UUIDgenerate()
        for (id in InstanceId) {
          # TODO Hacked by creating new namespace i believe since it's not working
          ui_id <- paste0(uuid, id)
          shiny$removeUI(
            selector = paste0("#", ui_id)
          )

          remove_shiny_inputs <- function(id, .input) {
            box::use(glue[glue])
            lapply(grep(id, names(.input), value = TRUE), function(i) {
              .subset2(.input, "impl")$.values$remove(i)
              is_removed <- i %in% .subset2(.input, "impl")$.values$keys()
              if (is_removed) print("removed") else "that didnt get removed"
            })
          }

          remove_shiny_inputs(ns(ui_id), input)

          shiny$insertUI(
            selector = paste0("#", ns("servers")),
            where = "beforeEnd",
            ec2$ui_ec2(ns(ui_id), id, instances, instanceTypes)
          )

          out <- ec2$server_ec2(ui_id, id, instances, aws_credentials = aws_credentials)
          out()
        }
      })
    }
  )
}
