#' @export
instance_types_get <- function() {
  box::use(rvest)
  box::use(janitor)
  box::use(dplyr)
  current_cpu <-
    dplyr$bind_rows(
      rvest$html_table(
        rvest$read_html(
          "https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/compute-optimized-instances.html"
        )
      )
    )
  instanceTypes <- unique(current_cpu$`Instance type`)

  instanceTypes <- instanceTypes[!is.na(instanceTypes)]
}
