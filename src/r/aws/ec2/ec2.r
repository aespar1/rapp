#' @export
ui_ec2 <- function(id = "ec2", InstanceId, instances, instanceTypes) {
  box::use(shiny = shiny[tags])
  box::use(shinyjs)
  box::use(glue = glue[glue])


  instance <- instances[instances$InstanceId == InstanceId, ]
  publicIpAddress <- instance$publicIpAddress
  ns <- shiny$NS(id)

  state <- instance$state
  tags$div(
    class = "col-12",
    tags$div(
      class='row',
      tags$div(
        class='col-sm-6',
        tags$a(target = "_blank", href = paste0("http://", instance$PublicIpAddress), glue("Public IP: {instance$PublicIpAddress}")),
        tags$p(glue("State: {state}"))
      ),
      tags$div(
        class='col-sm-6',
        tags$ul(
          tags$li(
            tags$a(target = "_blank", href = paste0("http://", instance$PublicIpAddress, ":8787"), "RStudio Server")
          ),
          tags$li(
            tags$a(target = "_blank", href = paste0("http://", instance$PublicIpAddress, ":3838"), "Shiny Server")
          ),
          tags$li(
            tags$a(target = "_blank", href = paste0("http://", instance$PublicIpAddress, ":61208"), "Glances")
          )
        )
      ),
      tags$div(
        class='col-12',
        tags$p('Connecting to your instance locally'),
        tags$pre(
          tags$code(
            glue$glue("sudo chmod 400 ~/.ssh/{instance$KeyName}.pem")
          ),
          tags$code(
            glue$glue("ssh -i \"~/.ssh/{instance$KeyName}.pem\" ubuntu@{instance$PublicDnsName}")
          )
        ),
        tags$p('Connecting to your instance dockerally'),
        tags$pre(
          tags$code(
            glue$glue("docker exec -it rapp chmod 400 /root/.ssh/{instance$KeyName}.pem")
          ),
          tags$code(
            glue$glue("docker exec -it rapp ssh -i \"/root/.ssh/{instance$KeyName}.pem\" ubuntu@{instance$PublicDnsName}")
          )
        ),
        tags$p('Adding a user'),
        tags$pre(
          tags$code(
            glue$glue("sudo adduser <username>")
          ),
          tags$code(
            glue$glue("sudo usermod -aG sudo -aG docker <username>")
          )
        ),
        tags$p('Get SSH Key for GIT'),
        tags$pre(
          tags$code(
            glue$glue("ssh-keygen")
          ),
          tags$code(
            glue$glue("cat ~/.ssh/id_rsa.pub")
          )
        ),
        tags$p('Set up https'),
        tags$pre(
          tags$code(
            glue$glue("sudo certbot --nginx -d <domain>.com -d www.<domain>.com")
          )
        )
      ),
      tags$div(
        class='col-sm-6',
        shiny$selectizeInput(ns("instanceType"), "Instance Type",
                                           unique(c(instanceTypes, instance$InstanceType)),
                                           instance$InstanceType,
                                           multiple = FALSE
        ),
        shiny$actionButton(ns("modify"), "Modify", class = "btn btn-sm btn-block btn-secondary")
      ),
      tags$div(
        class='col-sm-6',
        tags$div(
          class='d-grid gap-2',
          shiny$actionButton(ns("start"), "Start", class = "btn btn-sm btn-primary"),
          shiny$actionButton(ns("stop"), "Stop", class = "btn btn-sm  btn-warning"),
          shiny$actionButton(ns("reboot"), "Reboot", class = "btn btn-sm btn-warning"),
          shiny$actionButton(ns("terminate"), "Terminate", class = "btn btn-sm btn-danger")
        )
      )
    )
  )
}

#' @export
server_ec2 <- function(id = "ec2", InstanceId, instances, aws_credentials) {
  box::use(shiny = shiny[tags])
  box::use(glue)
  box::use(dplyr)
  box::use(.. / client[client])
  box::use(. / instance)
  box::use(jsonlite)
  box::use(../../connections/redis)
  # aws_credentials <- redis$get_state(shiny$NS('default')('aws_credentials'))
  ec2 <- client("ec2", aws_credentials = aws_credentials)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      instance <- shiny$reactive({
        instances <- instances[instances$InstanceId == InstanceId, ]
      })

      shiny$observeEvent(input$terminate, {
        tryCatch(
          {
            shiny$showNotification("Terminating instance")
            ec2$terminate_instances(InstanceIds = list(InstanceId))
            shiny$showNotification(shiny$p(shiny$icon("fire"), " wrekd"))
          },
          error = function(err) {
            shiny$showNotification(tags$pre(as.character(err)))
          }
        )
      })

      shiny$observeEvent(input$reboot, {
        tryCatch(
          {
            shiny$showNotification("Rebooting instance")
            ec2$reboot_instances(InstanceIds = list(InstanceId))
            shiny$showNotification(shiny$p(shiny$icon("fire"), "momentarily wrekd"))
          },
          error = function(err) {
            shiny$showNotification(tags$pre(as.character(err)))
          }
        )
      })


      shiny$observeEvent(input$modify, {
        tryCatch(
          {
            resp <- ec2$modify_instance_attribute(
              InstanceId = InstanceId,
              Attribute = "instanceType",
              Value = input$instanceType
            )
            shiny$showNotification(tags$p("I like it"))
          },
          error = function(err) {
            shiny$showNotification(tags$p(tags$code(as.character(err))))
          }
        )
      })

      shiny$observeEvent(input$stop, {
        shiny$showNotification("Stopping instance")
        ec2$stop_instances(InstanceIds = list(InstanceId))
        shiny$showNotification(shiny$p(
          shiny::icon("hand-peace"), "booped harder"
        ))
      })

      shiny$observeEvent(input$start, {
        tryCatch(
          {
            shiny$showNotification("Starting instance")
            ec2$start_instances(InstanceIds = list(InstanceId))
            shiny$showNotification(shiny$p(
              shiny::icon("hand-peace"), "booped"
            ))
          },
          error = function(err) {
            shiny$showNotification(shiny$p(
              shiny::icon("face-grin-beam-sweat"), " - probably just need to wait unless you're trying to start a terminated instance"
            ))
          }
        )
      })

      instance
    }
  )
}




#' @export
ec2_instance_create <- function(ImageId = "ami-097a2df4ac947655f",
                                InstanceType = "t2.medium",
                                min = 1,
                                max = 1,
                                KeyName = Sys.getenv("AWS_PEM"),
                                SecurityGroupId = Sys.getenv("AWS_SECURITY_GROUP"),
                                InstanceStorage = 10,
                                DeviceName = "/dev/sda1",
                                user_data = NA,
                                aws_credentials) {
  # box::use(paws[ec2])
  box::use(. / client)
  box::use(readr[read_file])
  box::use(. / state / updateState[updateState])
  box::use(base64[encode])
  box::use(utils[browseURL])
  box::use(. / s3)
  try(aws$ec2_instance_destroy(aws_credentials = aws_credentials))
  s3$s3_upload_proj(aws_credentials = aws_credentials)
  base_64_user_data <- read_file(encode("./shell/install_docker_ec2.sh"))
  ec2 <- client("ec2", aws_credentials = aws_credentials)
  response <-
    ec2$run_instances(
      ImageId = ImageId,
      InstanceType = InstanceType,
      MinCount = as.integer(min),
      MaxCount = as.integer(max),
      UserData = base_64_user_data,
      KeyName = KeyName,
      SecurityGroupIds = list(SecurityGroupId),
      BlockDeviceMappings = list(
        list(
          Ebs = list(
            VolumeSize = as.integer(InstanceStorage)
          ),
          DeviceName = DeviceName
        )
      )
    )
  instanceData <- list(response$Instances[[1]])
  InstanceId <- response$Instances[[1]]$InstanceId
  names(instanceData) <- InstanceId
  updateState(instanceData, "aws-ec2")
  # remote_public_ip <- getOption("domain")
  sleep_seconds <- 10
  #

  ready_for_association <- FALSE
  while (isFALSE(ready_for_association)) {
    ready_for_association <- tryCatch(
      "running" == ec2$describe_instance_status(InstanceId)[[1]][[1]]$InstanceState$Name,
      error = function(err) {
        print(err)
        Sys.sleep(1)
        FALSE
      }
    )
  }
  ec2$associate_address(
    InstanceId = InstanceId,
    PublicIp = getOption("ec2.nginx.conf")
  )
  system("pkill chrome")
  browseURL("https://us-east-2.console.aws.amazon.com/ec2/")
  browseURL(getOption("domain"))
  response
}

#' @export
ec2_instance_destroy <- function() {
  box::use(. / connections / storr)
  box::use(paws[ec2])
  ec2 <- ec2()
  con <- storr$connection_storr()
  ids <- con$get("aws-ec2")
  lapply(ids, function(x) {
    try(ec2$terminate_instances(x$InstanceId))
  })
  con$del("aws-ec2")
}

#' @export
ec2_instance_stop <- function() {
  box::use(. / connections / storr)
  box::use(paws[ec2])
  client <- box::use(. / aws / app / box / client)
  ec2 <- client$client("ec2")
  con <- storr$connection_storr()
  ids <- con$get("aws-ec2")
  lapply(ids, function(x) {
    try(ec2$stop_instances(x$InstanceId))
  })
}

#' @export
ec2_instance_start <- function() {
  box::use(. / connections / storr)
  box::use(paws[ec2])
  box::use(. / aws / app / box / client)
  ec2 <- client$client("ec2")
  con <- storr$connection_storr()
  ids <- con$get("aws-ec2")
  lapply(ids, function(x) {
    try(ec2$start_instances(x$InstanceId))
  })
}


#' @export
ec2_list_elastic_ip <- function(aws_credentials = NULL) {
  box::use(.. / client[client])
  box::use(purrr)
  box::use(. / ec2)
  # ec2$allocate_address(Domain='vpc')
  # ec2$associate_address()
  # ec2$describe_addresses()

  client_ec2 <- client("ec2", aws_credentials = aws_credentials)

  ipaddresses <- client_ec2$describe_addresses()
  elastic_ips <- purrr$map_dfr(
    ipaddresses$Addresses,
    function(x) {
      data.frame(
        InstanceId = ec2$if_is_null(x$InstanceId),
        PublicIp = ec2$if_is_null(x$PublicIp),
        Domain = ec2$if_is_null(x$Domain),
        NetworkBorderGroup = ec2$if_is_null(x$NetworkBorderGroup),
        AllocationId = ec2$if_is_null(x$AllocationId)
      )
    }
  )
  elastic_ips
}

#' @export
ec2_allocate_elastic_ip <- function(aws_credentials = NULL) {
  box::use(.. / client[client])
  box::use(purrr)
  box::use(. / ec2)

  ec2 <- client("ec2", aws_credentials = aws_credentials)

  elastic_ips <- ec2$allocate_address(Domain = "vpc")
  elastic_ips
}

#' @export
ec2_associate_elastic_ip <- function(aws_credentials = NULL, InstanceId, AllocationId) {
  box::use(.. / client[client])
  box::use(purrr)
  box::use(. / ec2)

  ec2 <- client("ec2", aws_credentials = aws_credentials)

  elastic_ips <- ec2$associate_address(AllocationId = AllocationId, InstanceId = InstanceId)
  elastic_ips
}

#' @export
ec2_release_elastic_ip <- function(aws_credentials = NULL, AllocationId) {
  box::use(.. / client[client])
  box::use(purrr)
  box::use(. / ec2)

  ec2 <- client("ec2", aws_credentials = aws_credentials)

  ec2$release_address(AllocationId = AllocationId)
}

#' @export
route53_list_hosted_zones <- function(aws_credentials = NULL) {
  box::use(.. / client[client])
  box::use(. / ec2)
  box::use(purrr)

  route53 <- client("route53", aws_credentials = aws_credentials)

  # response <- route53$create_hosted_zone(
  #   Name = "ndexr.io",
  #   CallerReference = "ndexr001"
  # )

  hosted_zones <- route53$list_hosted_zones()

  purrr$map_dfr(
    hosted_zones$HostedZones,
    function(x) {
      # print(x)
      data.frame(
        Id = ec2$if_is_null(x$Id),
        Name = ec2$if_is_null(x$Name),
        CallerReference = ec2$if_is_null(x$CallerReference),
        ConfigComment = ec2$if_is_null(x$Config$Comment),
        ConfigPrivateZone = ec2$if_is_null(x$Config$PrivateZone)
      )
    }
  )
}

#' @export
if_is_null <- function(x) {
  if (is.null(x)) {
    return(NA_character_)
  }
  x
}

#' @export
route53_update_hosted_zones <- function(aws_credentials = NULL, Domain = NULL, ElasticIp = NULL, HostedZoneId = NULL) {
  box::use(.. / client[client])

  box::use(purrr)

  route53 <- client("route53", aws_credentials = aws_credentials)


  purrr$map(
    Domain,
    function(x) {
      route53$change_resource_record_sets(
        ChangeBatch = list(
          "Changes" = list(
            list(
              Action = "UPSERT",
              ResourceRecordSet = list(
                Name = x,
                ResourceRecords = list(
                  list(
                    Value = ElasticIp
                  )
                ),
                TTL = 300L,
                Type = "A"
              )
            )
          ),
          Comment = "Web Server"
        ),
        HostedZoneId = HostedZoneId
      )
    }
  )
}

#' @export
ec2_elastic_ip_reset <- function() {
  box::use(. / ec2)
  box::use(purrr)
  box::use(. / instance)
  box::use(../.. / connections / redis)
  box::use(shiny)
  ns <- shiny$NS('default')
  aws_credentials <- redis$get_state(ns("aws_credentials"))

  instances <- instance$describe_instances(aws_credentials = aws_credentials)
  instances <- instances[instances$state=='running', ]
  elastic_ips <- ec2$ec2_list_elastic_ip(aws_credentials = aws_credentials)
  purrr$walk(elastic_ips$AllocationId, ~ ec2$ec2_release_elastic_ip(aws_credentials=aws_credentials, .))

  ec2$ec2_allocate_elastic_ip(aws_credentials = aws_credentials)
  elastic_ips <- ec2$ec2_list_elastic_ip(aws_credentials = aws_credentials)

  ec2$ec2_associate_elastic_ip(aws_credentials = aws_credentials,
                               InstanceId = instances$InstanceId[1],
                               elastic_ips$AllocationId[1])

  hosted_zones <- ec2$route53_list_hosted_zones(aws_credentials = aws_credentials)

  ec2$route53_update_hosted_zones(
    Domain = c("www.ndexr.com", "ndexr.com"),
    HostedZoneId = hosted_zones[hosted_zones$Name == "ndexr.com.", ]$Id,
    ElasticIp = elastic_ips$PublicIp[1],
    aws_credentials = aws_credentials
  )

  elastic_ips <- ec2$ec2_list_elastic_ip(aws_credentials = aws_credentials)
  elastic_ips
}
