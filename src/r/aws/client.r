#' @export
client <- function(service = NULL,
                   aws_credentials) {
  box::use(reticulate[import])
  client <- import("boto3")$client
  client <- client(service,
    aws_access_key_id = aws_credentials$awsAccess,
    aws_secret_access_key = aws_credentials$awsSecret,
    region_name = aws_credentials$defaultRegion
  )
}

#' @export
resource <- function(service = NULL,
                     aws_credentials) {
  box::use(reticulate[import])
  resource <- import("boto3")$resource
  resource <- resource(service,
    aws_access_key_id = aws_credentials$awsAccess,
    aws_secret_access_key = aws_credentials$awsSecret,
    region_name = aws_credentials$defaultRegion
  )
}
