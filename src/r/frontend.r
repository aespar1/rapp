#' @export
ui_frontend <- function(id) {
  {
    box::use(shiny = shiny[tags])
    box::use(shinyjs)
    box::use(. / sidebar / sidebar)
    box::use(. / router)
  }

  ns <- shiny$NS(id)
  shiny$addResourcePath(prefix = "javascript", directoryPath = "./javascript")
  shiny$addResourcePath(prefix = "css", directoryPath = "./css")

  tags$html(
    lang = "en",
    tags$head(
      tags$meta(charset = "utf-8"),
      tags$meta(name = "viewport", content = "width=device-width, initial-scale=1, shrink-to-fit=no"),
      tags$title("ndexr"),
      tags$meta(name = "author", content = "Freddy Drennan"),
      tags$meta(name = "description", content = "real good r"),
      tags$link(rel = "icon", type = "image/png", href = "./www/img/ndexrsym.png"),
      shinyjs$useShinyjs(),
      shiny$includeCSS("./css/styles.css", rel = "stylesheet"),
      tags$link(rel = "stylesheet", href = "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css")
    ),
    tags$body(
      id = "page-top",
      tags$div(
        class = "container-fluid",
        tags$div(
          class = "row min-vh-100",
          tags$nav(
            class = "col-sm-12 col-md-1 d-md-block bg-light sidebar sticky-top",
            sidebar$ui_sidebar(ns("sidebar"))
          ),
          tags$main(
            class = "col-sm-12 col-md-11",
            router$app_router(ns)$ui
          )
        )
      ),
      shiny$includeScript("../node_modules/d3/dist/d3.min.js"),
      shiny$includeScript("../node_modules/@popperjs/core/dist/umd/popper.min.js"),
      shiny$includeScript("../node_modules/bootstrap/dist/js/bootstrap.min.js"),
      shiny$includeScript("../node_modules/js-cookie/dist/js.cookie.min.js"),
      shiny$includeScript("./javascript/cookies.js"),
      shinyjs$extendShinyjs("./javascript/bgRandom.js", functions = "bgRandom")
    )
  )
}

#' @export
server_frontend <- function(id) {
  {
    box::use(shiny = shiny[withTags, tags])
    box::use(. / router)
    box::use(. / cookies / cookies)
    box::use(. / terminal / terminal)
    box::use(. / sidebar / sidebar)
    box::use(. / main / main)
    box::use(. / aws / aws)
    box::use(. / git / git)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      router$app_router(ns)$server(input, output, session)

      sidebar$server_sidebar("sidebar")
      aws$server_aws("aws")
      terminal$server_terminal("terminal")

      # git$server_gitlab_credentials("gitlab")
      cookies$server_cookies("cookies")
    }
  )
}
