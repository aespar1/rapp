#!/bin/bash
# Install docker
LOGFILE='/ndexrinstall.log'
savelog "$LOGFILE"
exec &> >(tee "$LOGFILE")
echo "alias tlog=\"tail -f /ndexrinstall.log\"" >> /home/ubuntu/.bashrc

{
  echo "#!/bin/bash"
  echo "sudo certbot --nginx -d ndexr.com -d www.ndexr.com"
  echo "make login"
  echo "docker compose up -d"
} >> /home/ubuntu/start

# update indices
apt update -qq
# install two helper packages we need
apt install --no-install-recommends software-properties-common dirmngr
# add the signing key (by Michael Rutter) for these repos
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# Fingerprint: E298A3A825C0D65DFD57CBB651716619E084DAB9
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# add the R 4.0 repo from CRAN -- adjust 'focal' to 'groovy' or 'bionic' as needed
add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"

apt install -y apt-transport-https ca-certificates curl nginx

wget https://BucketName.s3.us-east-2.amazonaws.com/nginx/etc/nginx/nginx.conf
mv nginx.conf /etc/nginx/nginx.conf
wget https://BucketName.s3.us-east-2.amazonaws.com/nginx/usr/share/nginx/html/index.html
mv index.html /usr/share/nginx/html/index.html

ufw allow 'Nginx Full'
systemctl restart nginx

apt install glances

cat <<EOT >> /etc/systemd/system/glances.service
[Unit]
Description=Glances
After=network.target

[Service]
ExecStart=/usr/bin/glances -w
Restart=on-abort
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOT

apt install nginx

sudo systemctl enable glances.service
sudo systemctl start glances.service

apt install -y apt-transport-https ca-certificates curl \
  software-properties-common gnupg lsb-release unzip \
  make nginx certbot python3-certbot-nginx \
  gdebi-core r-base r-base-dev libssl-dev zsh libcurl4-openssl-dev libgit2-dev \
  libxml2-dev cmake libcairo2-dev dirmngr libcurl4-openssl-dev libssh2-1-dev \
  zlib1g-dev make git-core libcurl4-openssl-dev libpq-dev cmake \
  libsodium-dev libsasl2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev \
  libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev default-jre default-jdk python3.10-venv \
  libhiredis-dev libssh-dev


mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update
apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
usermod -aG docker ubuntu

echo "deb http://security.ubuntu.com/ubuntu focal-security main" | sudo tee /etc/apt/sources.list.d/focal-security.list

sudo apt-get update
sudo apt-get install libssl1.1

sudo rm /etc/apt/sources.list.d/focal-security.list

# Install RStudio Server
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-2022.07.2-576-amd64.deb
gdebi -n rstudio-server-2022.07.2-576-amd64.deb

# Install R Stuff
R -e "install.packages(c('shiny', 'rmarkdown'))"

# Install Shiny Server
wget https://download3.rstudio.org/ubuntu-18.04/x86_64/shiny-server-1.5.19.995-amd64.deb
gdebi -n shiny-server-1.5.19.995-amd64.deb

R -e "install.packages(c('box', 'reticulate'))"

R -e "reticulate::install_miniconda()"

echo "deb http://security.ubuntu.com/ubuntu focal-security main" | sudo tee /etc/apt/sources.list.d/focal-security.list

# untested
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc

sudo apt install fontconfig
cd ~
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip
mkdir -p .local/share/fonts
unzip Meslo.zip -d .local/share/fonts
cd .local/share/fonts
rm *Windows*
cd ~
rm Meslo.zip
fc-cache -fv
