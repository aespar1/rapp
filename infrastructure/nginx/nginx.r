#' @export
nginx_wrap <- function(conf = NULL, name = "events") {
  paste0("\n\n", name, " {\n", conf, "\n}\n")
}

#' @export
readLinesTab <- function(x) {
  paste0("\t", readLines(x))
}

#' @export
nginx_head <- function() {
  nginx.head <- readLines("infrastructure/nginx/snippets/head.nginx.conf")
  nginx.head <- paste0(nginx.head, collapse = "\n")
}

#' @export
nginx_events <- function() {
  box::use(. / nginx)
  nginx.events <- nginx$readLinesTab("infrastructure/nginx/snippets/events.nginx.conf")
  nginx.events <- nginx$nginx_wrap(nginx.events, "events")
}

#' @export
nginx_http <- function() {
  box::use(. / nginx)
  http.nginx.top <- nginx$readLinesTab("infrastructure/nginx/snippets/http.nginx.top.conf")
  http.reverseproxy.conf <- nginx$readLinesTab("infrastructure/nginx/snippets/reverseproxy/http.reverseproxy.conf")
  http.nginx.bottom <- nginx$readLinesTab("infrastructure/nginx/snippets/http.nginx.bottom.conf")
  nginx.http <- paste0(c(http.nginx.top, http.reverseproxy.conf, http.nginx.bottom), collapse = "\n")
  nginx.http <- nginx$nginx_wrap(nginx.http, "http")
}

#' @export
nginx_finalize <- function(nginx.head, nginx.events, nginx.http) {
  nginx.conf <- paste0(nginx.head, nginx.events, nginx.http, collapse = "\n\n")
}

if (FALSE) {
  box::use(. / nginx)
  box::use(readr)
  out <- nginx$nginx_finalize(
    nginx$nginx_head(),
    nginx$nginx_events(),
    nginx$nginx_http()
  )

  readr$write_file(out, "infrastructure/nginx/nginx.conf")
}
